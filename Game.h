//
//  Game.h
//  pokerApp
//
//  Created by Eric Scagnelli on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

typedef enum{
    PRE_FLOP,
    FLOP,
    TURN,
    RIVER
}BETTING_ROUND;

@interface Game : NSObject{
    Player *currentPlayer;
    int playerIndex;
    unsigned int numPlayers;
    Player *firstPlayerInHand;
    int playerFirstIndex;
    NSString* winner;
}

@property (class, strong, nonatomic) Player *firstToBet;
@property (strong, nonatomic) NSMutableArray *players;
@property (nonatomic) int currentBet;
@property (nonatomic) float potValue;
@property (nonatomic) BETTING_ROUND bettingRound;

-(id)initWithPlayers:(NSMutableArray *) playersIn;
-(NSString *)nextPlayer;
-(void)nextFirstPlayer;
-(void)nextRound;
-(Player *)getCurrentPlayer;
-(void)setCurrentPlayerToFirstPlayer;

@end
