//
//  Chips.h
//  pokerApp
//
//  Created by Eric Scagnelli on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Chips : NSObject

@property (nonatomic) float value;
//@property (nonatomic) int numberOf;
@property (strong, nonatomic) UIImage *image;

+(float)whiteValue;
+(float)redValue;
+(float)blueValue;
+(float)greenValue;
+(float)blackValue;

//-(id)initWithNumber:(int) number;
-(id)initWithValue:(float)valueIn;

@end
