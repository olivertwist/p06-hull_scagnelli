//
//  Chips.m
//  pokerApp
//
//  Created by Eric Scagnelli on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import "Chips.h"

static int whiteValue = 1;
static int redValue = 5;
static int blueValue = 10;
static int greenValue = 25;
static int blackValue = 100;

@implementation Chips

+(float)whiteValue{
    return whiteValue;
}
+(float)redValue{
    return redValue;
}
+(float)blueValue{
    return blueValue;
}
+(float)greenValue{
    return greenValue;
}
+(float)blackValue{
    return blackValue;
}

-(id)initWithValue:(float)valueIn{
    self = [super init];
    if (self){
        _value = valueIn;
    }
    return self;
}

/*
-(id)initWithNumber:(int)number{
    self = [super init];
    if(self){
        _numberOf = number;
    }
    return self;
}
*/

/*
-(NSString *) description{
    switch ((int)self.value) {
        case 1:
            return [NSString stringWithFormat:@"White Chips x %d", self.numberOf];
            break;
        case 5:
            return [NSString stringWithFormat:@"Red Chips x %d", self.numberOf];
            break;
        case 10:
            return [NSString stringWithFormat:@"Blue Chips x %d", self.numberOf];
            break;
        case 25:
            return [NSString stringWithFormat:@"Green Chips x %d", self.numberOf];
            break;
        case 100:
            return [NSString stringWithFormat:@"Black Chips x %d", self.numberOf];
            break;
        default:
            return @"Oops";
            break;
    }
}
*/

@end
