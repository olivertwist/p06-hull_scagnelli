//
//  Game.m
//  pokerApp
//
//  Created by Eric Scagnelli on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import "Game.h"

@implementation Game
@synthesize players, currentBet, potValue;

static Player *_firstToBet = nil;

+(void)setFirstToBet:(Player *)playerIn{
    _firstToBet = playerIn;
}

+(Player *)firstToBet{
    if(_firstToBet == nil)
        _firstToBet = [[Player alloc] init];
    return _firstToBet;
}

-(id)initWithPlayers:(NSMutableArray *) playersIn{
    self = [super init];
    
    if(self){
        playerIndex = 0;
        playerFirstIndex = 0;
        potValue = 0;
        players = [[NSMutableArray alloc] initWithArray:playersIn];
        currentPlayer = players[playerIndex];  //First player is the first to bet
        firstPlayerInHand = players[playerFirstIndex];
        
        for (Player *player in players)
            player.isDealer = NO;
        Player *lastPlayer = players[[players count] - 1];  //The last player is the dealer first
        lastPlayer.isDealer = YES;
        numPlayers = (unsigned int)[players count];
    }
    
    return self;
}

//Moves to the next player
-(NSString *)nextPlayer{
    int originalPlayerIndex = playerIndex;
    winner = nil;
    do{
        playerIndex = (playerIndex + 1) % numPlayers;
        currentPlayer = players[playerIndex];
        if(playerIndex == originalPlayerIndex){ // Only one person left - they win
            NSLog(@"WINNEERRRR. Player %@ wins", currentPlayer.name);
            //Call pop up notice from here
            winner = currentPlayer.name;
            break;
        }
    }while(currentPlayer.didFold);
    return winner;
}

-(void)nextFirstPlayer{
    playerFirstIndex = (playerFirstIndex + 1) % numPlayers;
    firstPlayerInHand = players[playerFirstIndex];
}

-(void)nextRound{
    if(_bettingRound == PRE_FLOP)
        _bettingRound = FLOP;
    else if(_bettingRound == FLOP)
       _bettingRound = TURN;
    else if(_bettingRound == TURN)
       _bettingRound = RIVER;
    else if(_bettingRound == RIVER)
       _bettingRound = PRE_FLOP;
}

-(Player *)getCurrentPlayer{
    return currentPlayer;
}

-(void)setCurrentPlayerToFirstPlayer{
    //currentPlayer = firstPlayerInHand;
    for (int i = 0; i < numPlayers; i++){
        Player *player = players[i];
        if([player isEqualToPlayer:firstPlayerInHand]){
            playerIndex = i;
            currentPlayer = player;
            return;
        }
    }
}
    
/*
-(void)nextDealer{
    for(int i = 0; i < numPlayers; i++){
        Player *player = players[i];
        if(player.isDealer){
            player.isDealer = NO;
            Player *newDealer = players[(i+1) % numPlayers];
            newDealer.isDealer = YES;
        }
    }
}
*/

@end
