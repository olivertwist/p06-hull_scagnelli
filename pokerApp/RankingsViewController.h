//
//  RankingsViewController.h
//  pokerApp
//
//  Created by Eric Scagnelli on 4/16/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"

@interface RankingsViewController : UIViewController{
    Game *game;
}


-(void)displayRankings;
-(void)addLabel:(Player *)player atPoint:(CGPoint)point withRank:(int)rank;
-(void)setGame:(Game *)gameIn;

@end
