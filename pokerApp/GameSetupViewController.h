//
//  GameSetupViewController.h
//  pokerApp
//
//  Created by Eric Scagnelli on 4/10/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameSetupViewController : UIViewController{
    NSArray *playerLabels;
    NSArray *playerFields;
    int numPlayers;
}

@property (strong, nonatomic) IBOutlet UIButton *startBtn;
@property (strong, nonatomic) IBOutlet UILabel *numPlayersLabel;
@property (strong, nonatomic) IBOutlet UIStepper *stepper;

@property (strong, nonatomic) IBOutlet UILabel *player1Label;
@property (strong, nonatomic) IBOutlet UITextField *player1Field;
@property (strong, nonatomic) IBOutlet UILabel *player2Label;
@property (strong, nonatomic) IBOutlet UITextField *player2Field;
@property (strong, nonatomic) IBOutlet UILabel *player3Label;
@property (strong, nonatomic) IBOutlet UITextField *player3Field;
@property (strong, nonatomic) IBOutlet UILabel *player4Label;
@property (strong, nonatomic) IBOutlet UITextField *player4Field;
@property (strong, nonatomic) IBOutlet UILabel *player5Label;
@property (strong, nonatomic) IBOutlet UITextField *player5Field;
@property (strong, nonatomic) IBOutlet UILabel *player6Label;
@property (strong, nonatomic) IBOutlet UITextField *player6Field;
@property (strong, nonatomic) IBOutlet UILabel *player7Label;
@property (strong, nonatomic) IBOutlet UITextField *player7Field;
@property (strong, nonatomic) IBOutlet UILabel *player8Label;
@property (strong, nonatomic) IBOutlet UITextField *player8Field;
@property (strong, nonatomic) IBOutlet UITextField *startingMoneyField;


- (IBAction)displayNameFields:(id)sender;

@end
