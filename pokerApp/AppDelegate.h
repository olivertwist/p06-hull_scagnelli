//
//  AppDelegate.h
//  pokerApp
//
//  Created by oliver on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

