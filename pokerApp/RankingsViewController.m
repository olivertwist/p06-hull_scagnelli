//
//  RankingsViewController.m
//  pokerApp
//
//  Created by Eric Scagnelli on 4/16/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import "RankingsViewController.h"
#import "GameViewController.h"

@interface RankingsViewController ()

@end

@implementation RankingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self displayRankings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    GameViewController *destination = [segue destinationViewController];
    [game nextFirstPlayer];
    [game setCurrentPlayerToFirstPlayer];
    [Game setFirstToBet:game.getCurrentPlayer];
    game.currentBet = 0;
    game.potValue = 0;
    for (Player *player in game.players){ //At the beginning of each hand no one folded
        player.didFold = NO;
    }
    [destination setGame:game];
}

-(void)displayRankings{
    NSArray *sortedPlayers;
    sortedPlayers = [game.players sortedArrayUsingComparator:
                   ^NSComparisonResult(Player *a, Player *b){
                       float first = a.totalMoney;
                       float second = b.totalMoney;
                       return first < second;
                   }];
    
    NSLog(@"Game version is:");
    for (Player *player in game.players){
        NSLog(@"%@", player.name);
    }
    NSLog(@"sorted version is:");
    for (Player *player in sortedPlayers){
        NSLog(@"%@", player.name);
    }
    float xCenter = [self.view center].x;
    float leftRowCenterX = xCenter / 2.0;
    float rightRowCenterX = xCenter + xCenter / 2.0;
    float leftRowCenterY = self.view.frame.size.height / 3;
    float rightRowCenterY = leftRowCenterY;
    
    for(int i = 0; i < [sortedPlayers count]; i++){
        Player *player = sortedPlayers[i];
        if( i < 4){
            [self addLabel:player atPoint:CGPointMake(leftRowCenterX, leftRowCenterY)
             withRank:i+1];
            leftRowCenterY += 50;
        }
        else{
            [self addLabel:player atPoint:CGPointMake(rightRowCenterX, rightRowCenterY)
             withRank:i+1];
            rightRowCenterY += 50;
        }
    }
    
}

-(void)addLabel:(Player *)player atPoint:(CGPoint)point withRank:(int)rank{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 45)];
    label.font = [UIFont fontWithName:@"Avenir" size:35];
    label.text = [NSString stringWithFormat:@"%d. %@ $%d",rank, player.name, player.totalMoney];
    label.center = point;
    
    [self.view addSubview:label];
}

-(void)setGame:(Game *)gameIn{
    game = gameIn;
}

@end
