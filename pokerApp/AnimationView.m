//
//  AnimationView.m
//  pokerApp
//
//  Created by Eric Scagnelli on 4/17/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import "AnimationView.h"
#import "Game.h"

@implementation AnimationView



-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.backgroundColor = [UIColor clearColor]; //Make background transparent
        chips = [NSMutableArray arrayWithCapacity:50];
        potWhite = [NSMutableArray arrayWithCapacity:50];
        potRed = [NSMutableArray arrayWithCapacity:50];
        potBlue = [NSMutableArray arrayWithCapacity:50];
        potBlack = [NSMutableArray arrayWithCapacity:50];
        potGreen = [NSMutableArray arrayWithCapacity:50];
        
        whiteChipPos = CGPointMake(20,80);
        redChipPos = CGPointMake(62,80);
        blueChipPos = CGPointMake(104,80);
        greenChipPos = CGPointMake(146,80);
        blackChipPos = CGPointMake(188,80);
        
    }
    
    return self;
}

-(void)layoutImages{
    curPlayer = [game getCurrentPlayer];
    NSString *color;
    
    NSLog(@"Current chip val is %f", [curPlayer currentChipValue]);
    if([curPlayer currentChipValue] == 1.0){
        color = @"white_chip.png";
    }
    else if([curPlayer currentChipValue] == 5.0){
        color = @"red_chip.png";
    }
    else if([curPlayer currentChipValue] == 10.0){
        color = @"blue_chip.png";
    }
    else if([curPlayer currentChipValue] == 25.0){
        color = @"green_chip.png";
    }
    else if([curPlayer currentChipValue] == 100.0){
        color = @"black_chip.png";
    }
    float yPos = self.frame.size.height-16;
    
    int numChipsToDisplay;
    if(curPlayer.totalMoney > 20.0*[curPlayer currentChipValue]){
        numChipsToDisplay = 20;
    }
    else{
        numChipsToDisplay = floor(curPlayer.totalMoney/[curPlayer currentChipValue]);
    }
    NSLog(@"chips size: %lu", (unsigned long)[chips count]);
    
    for(int i = 0; i<[chips count]; i++){
        [[chips objectAtIndex:i] removeFromSuperview];
    }
    [chips removeAllObjects];
    
    chipPos = CGPointMake(self.frame.size.width/2.0, yPos);
    for(int i = 0; i<numChipsToDisplay; i++){
        UIImageView *chip = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        chip.center = chipPos;
        yPos-=4;
        chipPos = CGPointMake(self.frame.size.width/2.0, yPos);
        
        [chip setImage:[UIImage imageNamed:color]];
        [chip setContentMode:UIViewContentModeScaleAspectFit];
        [self addSubview:chip];
        
        [chips addObject:chip];
    }
    
    
}


-(void)removeChip{
    CGPoint pos;
    
    if([curPlayer currentChipValue] == 1.0){
        pos = whiteChipPos;
        whiteChipPos = CGPointMake(whiteChipPos.x, whiteChipPos.y-4);
        [potWhite addObject:[chips objectAtIndex:[chips count]-1]];
    }
    else if([curPlayer currentChipValue] == 5.0){
        pos = redChipPos;
        redChipPos = CGPointMake(redChipPos.x, redChipPos.y-4);
        [potRed addObject:[chips objectAtIndex:[chips count]-1]];
        
        
    }
    else if([curPlayer currentChipValue] == 10.0){
        pos = blueChipPos;
        blueChipPos = CGPointMake(blueChipPos.x, blueChipPos.y-4);
        [potBlue addObject:[chips objectAtIndex:[chips count]-1]];
    }
    else if([curPlayer currentChipValue] == 25.0){
        pos = greenChipPos;
        greenChipPos = CGPointMake(greenChipPos.x, greenChipPos.y-4);
        [potGreen addObject:[chips objectAtIndex:[chips count]-1]];
        
    }
    else if([curPlayer currentChipValue] == 100.0){
        pos = blackChipPos;
        blackChipPos = CGPointMake(blackChipPos.x, blackChipPos.y-4);
        [potBlack addObject:[chips objectAtIndex:[chips count]-1]];
    }
    
    
    [UIView animateWithDuration:.2f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        [self bringSubviewToFront:[chips objectAtIndex:[chips count]-1]];
        [[chips objectAtIndex:[chips count]-1] setCenter:pos];
        
    } completion:^(BOOL finished) {
        
    }];
    
    
    [chips removeObjectAtIndex:[chips count]-1];
    if([chips count] == 0){
        [self layoutImages];
    }

    chipPos = CGPointMake(chipPos.x, chipPos.y+4);

    
    
    
}


-(void)addChip{
    CGPoint pos;
    
    if([curPlayer currentChipValue] == 1.0){
        if([potWhite count] == 0) return;
        pos = chipPos;
        chipPos = CGPointMake(chipPos.x, chipPos.y-4);
        whiteChipPos = CGPointMake(whiteChipPos.x, whiteChipPos.y+4);
        
        [chips addObject:[potWhite objectAtIndex:[potWhite count]-1]];

        [UIView animateWithDuration:.2f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
            [self bringSubviewToFront:[potWhite objectAtIndex:[potWhite count]-1]];
            [[potWhite objectAtIndex:[potWhite count]-1] setCenter:pos];
            
        } completion:^(BOOL finished) {
            
        }];
        
        [potWhite removeObject:[potWhite objectAtIndex:[potWhite count]-1]];

    }
    else if([curPlayer currentChipValue] == 5.0){
        if([potRed count] == 0) return;
        pos = chipPos;
        chipPos = CGPointMake(chipPos.x, chipPos.y-4);
        redChipPos = CGPointMake(redChipPos.x, redChipPos.y+4);
        
        [chips addObject:[potRed objectAtIndex:[potRed count]-1]];
        
        [UIView animateWithDuration:.2f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
            [self bringSubviewToFront:[potRed objectAtIndex:[potRed count]-1]];
            [[potRed objectAtIndex:[potRed count]-1] setCenter:pos];
            
        } completion:^(BOOL finished) {
            
        }];
        
        [potRed removeObject:[potRed objectAtIndex:[potRed count]-1]];
        
    }
    else if([curPlayer currentChipValue] == 10.0){
        if([potBlue count] == 0) return;
        pos = chipPos;
        chipPos = CGPointMake(chipPos.x, chipPos.y-4);
        blueChipPos = CGPointMake(blueChipPos.x, blueChipPos.y+4);
        
        [chips addObject:[potBlue objectAtIndex:[potBlue count]-1]];
        
        [UIView animateWithDuration:.2f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
            [self bringSubviewToFront:[potBlue objectAtIndex:[potBlue count]-1]];
            [[potBlue objectAtIndex:[potBlue count]-1] setCenter:pos];
            
        } completion:^(BOOL finished) {
            
        }];
        
        [potBlue removeObject:[potBlue objectAtIndex:[potBlue count]-1]];
        
    }
    else if([curPlayer currentChipValue] == 25.0){
        if([potGreen count] == 0) return;
        pos = chipPos;
        chipPos = CGPointMake(chipPos.x, chipPos.y-4);
        greenChipPos = CGPointMake(greenChipPos.x, greenChipPos.y+4);
        
        [chips addObject:[potGreen objectAtIndex:[potGreen count]-1]];
        
        [UIView animateWithDuration:.2f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
            [self bringSubviewToFront:[potGreen objectAtIndex:[potGreen count]-1]];
            [[potGreen objectAtIndex:[potGreen count]-1] setCenter:pos];
            
        } completion:^(BOOL finished) {
            
        }];
        
        [potGreen removeObject:[potGreen objectAtIndex:[potGreen count]-1]];
        
    }
    
    else if([curPlayer currentChipValue] == 100.0){
        if([potBlack count] == 0) return;
        pos = chipPos;
        chipPos = CGPointMake(chipPos.x, chipPos.y-4);
        blackChipPos = CGPointMake(blackChipPos.x, blackChipPos.y+4);
        
        [chips addObject:[potBlack objectAtIndex:[potBlack count]-1]];
        
        [UIView animateWithDuration:.2f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
            [self bringSubviewToFront:[potBlack objectAtIndex:[potBlack count]-1]];
            [[potBlack objectAtIndex:[potBlack count]-1] setCenter:pos];
            
        } completion:^(BOOL finished) {
            
        }];
        
        [potBlack removeObject:[potBlack objectAtIndex:[potBlack count]-1]];
        
    }

    
}

-(void)setGame:(Game *)gameIn{
    game = gameIn;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


-(void)consolidateChips{
    if([potWhite count] > 14){
        [UIView animateWithDuration:.2f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^
         {
             for(int i = 0; i<[potWhite count];i++){
                 [self bringSubviewToFront:[potWhite objectAtIndex:i]];
                 [[potWhite objectAtIndex:i] setCenter:CGPointMake(self.frame.size.width/2, -60)];
             }
         } completion:^(BOOL finished) {
             for(int i = 0; i<[potWhite count];i++){
                 [[potWhite objectAtIndex:i] removeFromSuperview];
                 
             }
             [UIView animateWithDuration:.5f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^
              {
                  UIImageView *chip = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, -60, 40, 40)];
                  chip.center = redChipPos;
                  redChipPos = CGPointMake(redChipPos.x, redChipPos.y-4);
                  [chip setImage:[UIImage imageNamed:@"red_chip.png"]];
                  [chip setContentMode:UIViewContentModeScaleAspectFit];
                  [self addSubview:chip];
                  [potRed addObject:chip];
              }
                              completion:^(BOOL finished) {}];
             whiteChipPos = CGPointMake(20,80);
             [potWhite removeAllObjects];
             
         }];
    }
}

@end
