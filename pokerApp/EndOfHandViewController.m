//
//  EndOfHandViewController.m
//  pokerApp
//
//  Created by Eric Scagnelli on 4/16/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import "EndOfHandViewController.h"
#import "RankingsViewController.h"

@interface EndOfHandViewController ()

@end

@implementation EndOfHandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [UIView animateWithDuration:2 delay:2 options:UIViewAnimationOptionCurveEaseOut
                animations:^(void){
                    _goodLuckLabel.alpha = 0;
                }
                completion:^(BOOL finished){
                    _goodLuckLabel.hidden = YES;
                    [_goodLuckLabel removeFromSuperview];
                    [self displayPossibleWinners];
                }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)displayPossibleWinners{
    UILabel *whoWonLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 100)];
    whoWonLabel.font = [UIFont fontWithName:@"Poker Kings" size:40];
    whoWonLabel.text = @"Who Won?";
    [whoWonLabel setTextAlignment:NSTextAlignmentCenter];
    float xCenter = self.view.center.x;
    float yCenter = 60;
    whoWonLabel.center = CGPointMake(xCenter, yCenter);
    
    [self.view addSubview:whoWonLabel];
    
    float leftRowCenterX = xCenter / 2.0;
    float rightRowCenterX = xCenter + xCenter / 2.0;
    float leftRowCenterY = self.view.frame.size.height / 3;
    float rightRowCenterY = leftRowCenterY;
    
    for(int i = 0; i < [game.players count]; i++){
        Player *player = game.players[i];
        if(!player.didFold){
            if( i < 4){
                [self addButton:player atPoint:CGPointMake(leftRowCenterX, leftRowCenterY)];
                leftRowCenterY += 60;
            }
            else{
                [self addButton:player atPoint:CGPointMake(rightRowCenterX, rightRowCenterY)];
                rightRowCenterY += 60;
            }
        }
    }
}

-(void)addButton:(Player *)player atPoint:(CGPoint)point{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
    btn.backgroundColor = [UIColor colorWithRed:128.0/255.0 green:0 blue:0 alpha:1];
    btn.titleLabel.font = [UIFont fontWithName:@"Poker Kings" size:35];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:player.name forState:UIControlStateNormal];
    [btn setTintColor:[UIColor blackColor]];
    btn.layer.cornerRadius = 15;
    btn.center = point;
    
    btn.accessibilityHint = [NSString stringWithFormat:@"%f", game.potValue];
    [btn addTarget:player action:@selector(takePot:) forControlEvents:UIControlEventTouchUpInside];
    [btn addTarget:self action:@selector(presentRankingsController) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn];
}

-(void)setGame:(Game *)gameIn{
    game = gameIn;
}

-(void)presentRankingsController{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    RankingsViewController *rankingsController = [storyBoard instantiateViewControllerWithIdentifier:@"rankingsController"];
    [rankingsController setGame:game];
    [self presentViewController:rankingsController animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
