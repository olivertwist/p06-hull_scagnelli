//
//  EndOfHandViewController.h
//  pokerApp
//
//  Created by Eric Scagnelli on 4/16/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"

@interface EndOfHandViewController : UIViewController{
    Game *game;
}

@property(strong,nonatomic) IBOutlet UILabel *goodLuckLabel;

-(void)displayPossibleWinners;
-(void)addButton:(Player *)player atPoint:(CGPoint)point;
-(void)setGame:(Game *)gameIn;
-(void)presentRankingsController;

@end
