//
//  Player.h
//  pokerApp
//
//  Created by Eric Scagnelli on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Chips.h"

@interface Player : NSObject

@property (nonatomic) bool isDealer;
@property (strong, nonatomic) NSMutableArray *chips;
@property (nonatomic) int totalMoney; //chips will only be in whole number denominations
@property (strong, nonatomic) NSString *name;
@property (nonatomic) float currentChipValue;
@property (nonatomic) bool didFold;

//-(void)initalChipDistribution;
-(Chips *) getCurrentChip;
-(BOOL) isEqualToPlayer:(Player *)otherPlayer;
-(void)takePot:(UIButton *)sender;

@end
