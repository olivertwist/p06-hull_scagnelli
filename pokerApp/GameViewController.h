//
//  GameViewController.h
//  pokerApp
//
//  Created by oliver on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameplayKit/GameplayKit.h>
#import "Game.h"
#import "AnimationView.h"

@interface GameViewController : UIViewController <UITextFieldDelegate>{
    Game *game;
    bool bettingStarted;
}

@property (strong, nonatomic) UISwipeGestureRecognizer *leftSwipe;
@property (strong, nonatomic) UISwipeGestureRecognizer *rightSwipe;
@property (strong, nonatomic) UISwipeGestureRecognizer *upSwipe;
@property (strong, nonatomic) UISwipeGestureRecognizer *downSwipe;

@property (strong, nonatomic) IBOutlet UITextField *playersBet;
@property (strong, nonatomic) IBOutlet UIStepper *stepper;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalMoney;
@property (strong, nonatomic) IBOutlet UILabel *currentBet;
@property (strong, nonatomic) IBOutlet UIButton *callBtn;
@property (strong, nonatomic) IBOutlet UIButton *checkBtn;
@property (strong, nonatomic) IBOutlet UIButton *foldBtn;
@property (strong, nonatomic) IBOutlet UIButton *betBtn;
@property (strong, nonatomic) IBOutlet UILabel *bettingRoundLabel;

@property (strong, nonatomic) IBOutlet AnimationView *animationView;



-(void) displayNewChips;
-(void) setGame:(Game *) gameIn;
-(void)nextPlayer;
-(IBAction)changeBet:(id)sender;
-(IBAction)call:(id)sender;
-(IBAction)check:(id)sender;
-(IBAction)fold:(id)sender;
-(IBAction)bet:(id)sender;
-(void) increaseBetBy:(float)amount;
-(void) decreaseBetBy:(float)amount;
-(void) insufficientFundsAlert;
-(void)setButtonStates;
-(IBAction)resumeGame:(UIStoryboardSegue *)segue;
-(void)handOver;
-(void) setRoundLabel;

@end
