//
//  AnimationView.h
//  pokerApp
//
//  Created by Eric Scagnelli on 4/17/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"


@interface AnimationView : UIView {
    Game* game;
    Player* curPlayer;
    NSMutableArray* chips;
    NSMutableArray* potWhite;
    NSMutableArray* potRed;
    NSMutableArray* potGreen;
    NSMutableArray* potBlue;
    NSMutableArray* potBlack;
    
    
    
    CGPoint chipPos;
    
    CGPoint whiteChipPos;
    CGPoint redChipPos;
    CGPoint blackChipPos;
    CGPoint blueChipPos;
    CGPoint greenChipPos;



    
}

-(void)layoutImages;
-(void)removeChip;
-(void)addChip;
-(void)setGame:(Game *)gameIn;


@end
