//
//  GameViewController.m
//  pokerApp
//
//  Created by oliver on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "EndOfHandViewController.h"
#import "AnimationView.h"
#import "RankingsViewController.h"

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:tap];
    
    _playersBet.delegate = self;
    _nameLabel.text = [NSString stringWithFormat:@"%@'s\nTurn", [game getCurrentPlayer].name];
    _totalMoney.text = [NSString stringWithFormat:@"Total Money:\n$%d", [[game getCurrentPlayer] totalMoney]];
    _currentBet.text = [NSString stringWithFormat:@"$%d\nTo You", game.currentBet];
    
    self.leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    self.rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    self.upSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    self.downSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    
    self.leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    self.upSwipe.direction = UISwipeGestureRecognizerDirectionUp;
    self.downSwipe.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.view addGestureRecognizer:self.leftSwipe];
    [self.view addGestureRecognizer:self.rightSwipe];
    [self.view addGestureRecognizer:self.upSwipe];
    [self.view addGestureRecognizer:self.downSwipe];
    
    [self disableButton:_betBtn];
    bettingStarted = NO;
    [self setRoundLabel];
    
    [_animationView setGame:game];
    [_animationView layoutImages];
    
}
 

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}



- (void)swipeHandler:(UISwipeGestureRecognizer *)sender{
    
    float chipValues[] = {[Chips whiteValue], [Chips redValue], [Chips blueValue], [Chips greenValue], [Chips blackValue]};
    int chipValSize = sizeof(chipValues) / sizeof(float);
    
    float currentChipValue;
    int index;
    float newChipsValue;
    
    switch(sender.direction){
        //Increase the value of the chip or loop around if at the highest value
        case UISwipeGestureRecognizerDirectionLeft:
            NSLog(@"Left swipe");
            currentChipValue = game.getCurrentPlayer.currentChipValue;
            index = [self getChipIndex:chipValues withValue:currentChipValue arraySize:chipValSize];
            if (index == chipValSize - 1)
                index = 0;
            else
                index++;
            newChipsValue = chipValues[index];
            game.getCurrentPlayer.currentChipValue = newChipsValue;
            [_animationView layoutImages];
            break;
            
        //Decrease the value of the chip or loop around if at the lowest value
        case UISwipeGestureRecognizerDirectionRight:
            NSLog(@"Right swipe");
            currentChipValue = game.getCurrentPlayer.currentChipValue;
            index = [self getChipIndex:chipValues withValue:currentChipValue arraySize:chipValSize];
            if (index == 0)
                index = chipValSize - 1;
            else
                index--;
            newChipsValue = chipValues[index];
            game.getCurrentPlayer.currentChipValue = newChipsValue;
            [_animationView layoutImages];

            break;
            
        //Put 1 of the current chip in the pot
        case UISwipeGestureRecognizerDirectionUp:
            NSLog(@"Up Swipe");
            [self increaseBetBy:game.getCurrentPlayer.currentChipValue];
            //[_animationView removeChip];
            
            break;
            
        //Take 1 of the current chip out of the pot if you have enough in the pot
        case UISwipeGestureRecognizerDirectionDown:
            NSLog(@"Down Swipe");
            [self decreaseBetBy:game.getCurrentPlayer.currentChipValue];
            //[_animationView addChip];

            break;
    }

    [self setButtonStates];
}

-(int)getChipIndex:(float *)chipValues withValue:(float)value arraySize:(int)size{
    for(int i = 0; i < size; i++){
        if(chipValues[i] == value)
            return i;
    }
    return -1;
}

-(IBAction)changeBet:(id)sender{
    bool validBet = NO;
    bool increasingBet;
    int oldTotalMoney = game.getCurrentPlayer.totalMoney;
    int oldPotValue = (int)game.potValue;
    
    if(_stepper.value > [_playersBet.text intValue]){ //Increased bet
        game.getCurrentPlayer.totalMoney -= game.getCurrentPlayer.currentChipValue;
        game.potValue += game.getCurrentPlayer.currentChipValue;
        _stepper.value += game.getCurrentPlayer.currentChipValue - 1;
        increasingBet = true;
    }
    else{ //Decrease bet
        game.getCurrentPlayer.totalMoney += game.getCurrentPlayer.currentChipValue;
        game.potValue -= game.getCurrentPlayer.currentChipValue;
        _stepper.value -= game.getCurrentPlayer.currentChipValue - 1;
        increasingBet = false;
    }
    
    if(game.getCurrentPlayer.totalMoney >= 0)
        validBet = YES;
    
    //Update labels
    if(validBet){
        _playersBet.text = [NSString stringWithFormat:@"%d", (int)_stepper.value];
        _totalMoney.text = [NSString stringWithFormat:@"Total Money:\n$%d", [[game getCurrentPlayer] totalMoney]];
        [self updatePotValue];
        if(increasingBet) [_animationView removeChip];
        else [_animationView addChip];
        

    }
    
    else{
        game.getCurrentPlayer.totalMoney = oldTotalMoney;  //Reset players money
        _stepper.value -= game.getCurrentPlayer.currentChipValue;  //undo the increase to the stepper
        game.potValue = oldPotValue;
        [self insufficientFundsAlert];
    }
    
    [self setButtonStates];
}

-(void) setGame:(Game *) gameIn{
    game = gameIn;
}

-(void) setRoundLabel{
    if(game.bettingRound == PRE_FLOP)
        self.bettingRoundLabel.text = @"PRE-FLOP";
    else if(game.bettingRound == FLOP)
        self.bettingRoundLabel.text = @"FLOP";
    else if(game.bettingRound == TURN)
        self.bettingRoundLabel.text = @"TURN";
    else if(game.bettingRound == RIVER)
        self.bettingRoundLabel.text = @"RIVER";
}

-(void)nextPlayer{
    if([game nextPlayer] != nil){
        [self presentRankingsController];
    }
    [_animationView layoutImages];
    NSLog(@"Game current player is now %@", game.getCurrentPlayer.name);
    NSLog(@"Static first to bet is %@", [Game firstToBet].name);
    if([game.getCurrentPlayer isEqualToPlayer:[Game firstToBet]] && game.bettingRound == RIVER){ //hand is over
        [game nextRound];
        [self handOver];
    }
    else if([game.getCurrentPlayer isEqualToPlayer:[Game firstToBet]]){ //Betting round is over
        [game nextRound];
        game.currentBet = 0;
        _playersBet.text = @"0";
    }
    
    [self setRoundLabel];
    
    NSLog(@"Current player is now %@", [game getCurrentPlayer].name);
    
    //Display the new players info on the screen
    _nameLabel.text = [NSString stringWithFormat:@"%@'s\nTurn", [game getCurrentPlayer].name];
    _totalMoney.text = [NSString stringWithFormat:@"Total Money:\n$%d", [[game getCurrentPlayer] totalMoney]];
    if([_playersBet.text intValue] > game.currentBet){
        NSLog(@"Players bet is %d", [_playersBet.text intValue]);
        game.currentBet += [_playersBet.text intValue] - game.currentBet;
    }
    _currentBet.text = [NSString stringWithFormat:@"$%d\nTo You", game.currentBet];
    _playersBet.text = @"0";
    _stepper.value = 0;
//    _chips.backgroundColor = [UIColor whiteColor];
    
    [self setButtonStates];
}

- (IBAction)call:(id)sender {
    [self nextPlayer];
}

- (IBAction)check:(id)sender {
    [self nextPlayer];
}

- (IBAction)fold:(id)sender{
    game.getCurrentPlayer.didFold = YES;
    [self nextPlayer];
}

-(IBAction)bet:(id)sender{
    if(!bettingStarted){
        Game.firstToBet = game.getCurrentPlayer;
        bettingStarted = YES;
    }
    else if([_playersBet.text intValue] > game.currentBet) //Someone raised
        Game.firstToBet = game.getCurrentPlayer; //Need to go around until we get back to this new person
    
    [self nextPlayer];
}

-(void) increaseBetBy:(float)amount{
    if(amount <= game.getCurrentPlayer.totalMoney){
        game.potValue += game.getCurrentPlayer.currentChipValue;
        _playersBet.text = [NSString stringWithFormat:@"%d", [_playersBet.text intValue] + (int)amount];
        _stepper.value = [_playersBet.text intValue];
        game.getCurrentPlayer.totalMoney -= amount;
        _totalMoney.text = [NSString stringWithFormat:@"Total Money:\n$%d", [[game getCurrentPlayer] totalMoney]];
        [_animationView removeChip];
        
        [self updatePotValue];
    }
    else{ //Player doesn't have enough money to put this in pot
        [self insufficientFundsAlert];
    }
}

-(void) decreaseBetBy:(float)amount{
    
    if([_playersBet.text intValue] - amount >= 0){
        game.potValue -= game.getCurrentPlayer.currentChipValue;
        _playersBet.text = [NSString stringWithFormat:@"%d", [_playersBet.text intValue] - (int)amount];
        _stepper.value = [_playersBet.text intValue];
        game.getCurrentPlayer.totalMoney += amount;
        _totalMoney.text = [NSString stringWithFormat:@"Total Money:\n$%d", [[game getCurrentPlayer] totalMoney]];
        [_animationView addChip];
    }
    
    [self updatePotValue];

}

-(IBAction)dismissKeyboard:(id)sender{
    [self.view endEditing:YES];
//    [self textFieldDidEndEditing:_playersBet]; //Shouldn't have to manually call this but the delegate method wasn't getting called for some reason
}

/*
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if([textField.text isEqualToString:@""]){
        _stepper.value = 0;
        textField.text = [NSString stringWithFormat:@"%d", 0];
    }
    else{
        _stepper.value = [textField.text intValue];
    }
}
*/
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}

-(void) updatePotValue{
//    _pot.text = [NSString stringWithFormat:@"%d", (int)game.potValue];
}

-(void) insufficientFundsAlert{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Insufficient Funds!"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okBtn = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                    }];
    
    [alert addAction:okBtn];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) winnerAlert{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"You Won boi!"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okBtn = [UIAlertAction
                            actionWithTitle:@"OK"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action) {
                            }];
    
    [alert addAction:okBtn];
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)setButtonStates{
    if(game.currentBet > 0 || [_playersBet.text intValue] > 0)
        [self disableButton:_checkBtn];
    else
        [self enableButton:_checkBtn];
    
    if([_playersBet.text intValue] == game.currentBet)
        [self enableButton:_callBtn];
    else
        [self disableButton:_callBtn];
    
    if([_playersBet.text intValue] > game.currentBet)
        [self enableButton:_betBtn];
    else
        [self disableButton:_betBtn];
}

-(void)disableButton:(UIButton *)btn{
    btn.enabled = NO;
    btn.alpha = .5;
}

-(void)enableButton:(UIButton *)btn{
    btn.enabled = YES;
    btn.alpha = 1;
}

//Empty method just to get back to view
-(IBAction)resumeGame:(UIStoryboardSegue *)segue{
}

-(void)handOver{
    NSLog(@"THE HAND IS OVERRRR");
    //Transition to another view controller to find out who the winner was
    //Give the winner the pot
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    EndOfHandViewController *handOverController = [storyBoard instantiateViewControllerWithIdentifier:@"HandOverController"];
    [handOverController setGame:game];
    [self presentViewController:handOverController animated:YES completion:nil];
}



-(void)presentRankingsController{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    RankingsViewController *rankingsController = [storyBoard instantiateViewControllerWithIdentifier:@"rankingsController"];
    [rankingsController setGame:game];
    [self presentViewController:rankingsController animated:YES completion:nil];
}


@end
