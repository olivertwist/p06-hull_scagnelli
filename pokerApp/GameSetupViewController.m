//
//  GameSetupViewController.m
//  pokerApp
//
//  Created by Eric Scagnelli on 4/10/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import "GameSetupViewController.h"
#import "GameViewController.h"

#define MAX_PLAYERS 8


@interface GameSetupViewController ()

@end

@implementation GameSetupViewController
@synthesize startBtn, numPlayersLabel, stepper;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self disableStartBtn];
    
    //Dismiss keyboard when they tap other parts of screen
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:tap];
    
    numPlayers = 2;
    playerLabels = @[_player1Label, _player2Label, _player3Label, _player4Label, _player5Label,
                     _player6Label, _player7Label, _player8Label];
    playerFields = @[_player1Field, _player2Field, _player3Field, _player4Field, _player5Field,
                     _player6Field, _player7Field, _player8Field];
    
    for(int i = 0; i < MAX_PLAYERS; i++){
        UILabel *playerLabel = playerLabels[i];
        UITextField *playerField = playerFields[i];
        
        if (i == 0 || i == 1){
            playerLabel.hidden = NO;
            playerField.hidden = NO;
        }
        else{
            playerLabel.hidden = YES;
            playerField.hidden = YES;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
//Can't get called until the start button is pressed and all info is entered
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSMutableArray *players = [[NSMutableArray alloc] init];
    
    //Add each player with name, starting money amount, chips
    for (int i = 0; i < numPlayers; i++){
        Player *player = [[Player alloc] init];
        UITextField *playerField = playerFields[i];
        player.name = playerField.text;
        player.totalMoney = [_startingMoneyField.text intValue];
        
        [players addObject:player];
    }
    
    Game *game = [[Game alloc]initWithPlayers:players];
    game.currentBet = 0;
    
    GameViewController *destination = [segue destinationViewController];
    [destination setGame:game];
}

- (IBAction)displayNameFields:(id)sender {
    numPlayers = (int)stepper.value;
    [self stepperChanged:numPlayers];
    
    for(int i = 0; i < MAX_PLAYERS; i++){
        UILabel *playerLabel = playerLabels[i];
        UITextField *playerField = playerFields[i];
        
        if (i < numPlayers){
            playerLabel.hidden = NO;
            playerField.hidden = NO;
        }
        else{
            playerLabel.hidden = YES;
            playerField.hidden = YES;
        }
    }
    
    bool allInfo = [self allInfoEntered];
    if(allInfo)
        [self enableStartBtn];
    else
        [self disableStartBtn];
}

-(void) stepperChanged:(int) numberPlayers{
    switch (numberPlayers){
        case 1:
            numPlayersLabel.text = @"one";
            break;
        case 2:
            numPlayersLabel.text = @"two";
            break;
        case 3:
            numPlayersLabel.text = @"three";
            break;
        case 4:
            numPlayersLabel.text = @"four";
            break;
        case 5:
            numPlayersLabel.text = @"five";
            break;
        case 6:
            numPlayersLabel.text = @"six";
            break;
        case 7:
            numPlayersLabel.text = @"seven";
            break;
        case 8:
            numPlayersLabel.text = @"eight";
            break;
        default:
            break;
    }
}

-(IBAction)dismissKeyboard:(id)sender{
    [self.view endEditing:YES];
    
    bool allInfo = [self allInfoEntered];
    if(allInfo)
        [self enableStartBtn];
    else
        [self disableStartBtn];
}

-(bool)allInfoEntered{
    //Check that each player has a name entered
    for(int i = 0; i < numPlayers; i++){
        UITextField *playerField = playerFields[i];
        if (![playerField hasText])
            return NO;
    }
    
    //Check for a starting money amount
    if(![_startingMoneyField hasText])
        return NO;
    
    return YES;
}

-(void)disableStartBtn{
    startBtn.enabled = NO;
    startBtn.alpha = .5;
}

-(void)enableStartBtn{
    startBtn.enabled = YES;
    startBtn.alpha = 1;
}

@end
