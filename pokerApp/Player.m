//
//  Player.m
//  pokerApp
//
//  Created by Eric Scagnelli on 4/4/17.
//  Copyright © 2017 oliver. All rights reserved.
//

#import "Player.h"

@implementation Player
@synthesize totalMoney;


-(id)init{
    self = [super init];
    if(self){
        _chips = [[NSMutableArray alloc] init];
        _currentChipValue = 1;  //Players will start with white chips in front of them
        
        //Set up value of the chips
        Chips *whiteChips = [[Chips alloc] initWithValue:1];
        Chips *redChips = [[Chips alloc] initWithValue:5];
        Chips *blueChips = [[Chips alloc] initWithValue:10];
        Chips *greenChips = [[Chips alloc] initWithValue:25];
        Chips *blackChips = [[Chips alloc] initWithValue:100];
        
        //Add chips to chip array
        [_chips addObject:whiteChips];
        [_chips addObject:redChips];
        [_chips addObject:blueChips];
        [_chips addObject:greenChips];
        [_chips addObject:blackChips];
        
        _didFold = NO;
    }
    return self;
}

-(Chips *) getCurrentChip{
    if(_currentChipValue == 1)
        return _chips[0];
    else if (_currentChipValue == 5)
        return _chips[1];
    else if (_currentChipValue == 10)
        return _chips[2];
    else if (_currentChipValue == 25)
        return _chips[3];
    else if (_currentChipValue == 100)
        return _chips[4];
    else
        return nil;
}

-(BOOL)isEqualToPlayer:(Player *)otherPlayer{
    return _isDealer == otherPlayer.isDealer &&
        totalMoney == otherPlayer.totalMoney && [_name isEqualToString:otherPlayer.name];
}

-(void)takePot:(UIButton *)sender{
    totalMoney += [sender.accessibilityHint floatValue];
    NSLog(@"Player %@ now has %d", _name, totalMoney);
}

/*
-(void)initalChipDistribution{
    int numWhiteChips = 0;  //$1
    int numRedChips = 0;    //$5
    int numBlueChips = 0;   //$10
    int numGreenChips = 0;  //$25
    int numBlackChips = 0;  //$100
    
    //Make sure they start with some smaller chips
    //First $20 will be 1 blue, 2 red, 5 white
    if (totalMoney <= [Chips redValue]){
        numWhiteChips = totalMoney;
    }
    else if (totalMoney <= [Chips blueValue]){
        numRedChips = 1;
        numWhiteChips = totalMoney - [Chips redValue];
    }
    else if (totalMoney <= [Chips blueValue] + [Chips redValue]){
        numRedChips = 2;
        numWhiteChips = totalMoney - [Chips blueValue];
    }
    else if (totalMoney <= [Chips blueValue] * 2){
        numBlueChips = 1;
        numRedChips = 2;
        numWhiteChips = totalMoney - [Chips blueValue] - [Chips redValue];
    }
    
    int moneyLeft = totalMoney - numBlueChips * [Chips blueValue] - numRedChips * [Chips redValue] - numWhiteChips;
    
    //Now that they have $20 of smaller chips use the largest chips possible for the rest
    numBlackChips = moneyLeft / [Chips blackValue];
    moneyLeft %= [Chips blackValue];
    numGreenChips = moneyLeft / [Chips greenValue];
    moneyLeft %= [Chips greenValue];
    numBlueChips += (moneyLeft / [Chips blueValue]);
    moneyLeft %= [Chips blueValue];
    numRedChips += (moneyLeft / [Chips redValue]);
    moneyLeft %= [Chips redValue];
    numWhiteChips += moneyLeft;
    
    Chips *whiteChips = [[Chips alloc] initWithNumber:numWhiteChips];
    whiteChips.value = [Chips whiteValue];
    [_chips addObject:whiteChips];
    Chips *redChips = [[Chips alloc] initWithNumber:numRedChips];
    redChips.value = [Chips redValue];
    [_chips addObject:redChips];
    Chips *blueChips = [[Chips alloc] initWithNumber:numBlueChips];
    blueChips.value = [Chips blueValue];
    [_chips addObject:blueChips];
    Chips *greenChips = [[Chips alloc] initWithNumber:numGreenChips];
    greenChips.value = [Chips greenValue];
    [_chips addObject:greenChips];
    Chips *blackChips = [[Chips alloc] initWithNumber:numBlackChips];
    blackChips.value = [Chips blackValue];
    [_chips addObject:blackChips];
}
*/

- (NSString *) description{
    return [NSString stringWithFormat:@"Name: %@\nTotal Money %d\n"
                "%@\n%@\n%@\n%@\n%@",
                    self.name, self.totalMoney, self.chips[0], self.chips[1],
                    self.chips[2], self.chips[3], self.chips[4]];
}


@end
